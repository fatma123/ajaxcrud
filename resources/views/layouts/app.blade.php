<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel Crud</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <nav class="navbar navbar-default navbar-ststic-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">CodELog</a>
            </div>
        </div>
    </nav> 

    <div class="container">
        @yield('content')
    </div>
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


        {{-- Ajax Form Add--}}
        <script type="text/javascript">
            $(document).on('click','.create-modal', function() {
                $('#create').modal('show');
                $('.form-horizontal').show();
                $('.modal-title').text('Add Post');
            });
            {{-- Ajax Form store post--}}
          $('#add').click(function(){
                $.ajax({
                   type: 'Post',
                   url:'posts',
                    data:{
                       '_token': $('input[name=_token]').val(),
                        'title': $('input[name=title]').val(),
                        'body' : $('input[name=body]').val()
                    },
                    success:function(data){

                       if ((data.errors)){
                           $('.error').removeClass('hidden');
                           $('.error').text('data.errors.title');
                           $('.error').text('data.errors.body');
                       }else {

                       }
                    }
                });

            });

            {{-- Ajax Form show--}}
            $(document).on('click','.show-modal', function() {
                $('#show').modal('show');
                $('.modal-title').text('show-post');
            });

        </script>


</body>
</html>



</html>